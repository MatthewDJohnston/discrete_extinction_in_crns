import sys
import os
sys.path.insert(0, os.path.join(
    os.path.dirname(os.path.realpath(__file__)), 'crn_python'))

from pulp import *
import numpy as np
from crn import *
from DE_Modules import *

# Import Data File

filename = "example_envz_ompr"
crn = from_react_file(filename)
eps = 0.1
		
(ns,nc,nr,Y,Ia,Ik,N,Ak) = CreateModel(crn)
	
# Print to file
file = open(filename + ".dat","w+")
#file = open("test.dat","w+")

# Check if network is conservative
(IsSubconservative,IsConservative) = CheckSubconservative(ns,nc,nr,Y,Ia,N,file)

# If network is conservative, check for discrete extinction
if IsSubconservative:

	# Find Domination Set D*
	DomStar = DominationSet(ns,nc,Y)
	
	# Initialize X* as empty set
	XStar = [0 for i in range(nc)]
	
	# Find Initial Absorbing Set X and Admissible D
	(X,Dom) = FindAdmissibleDom(nr,ns,nc,XStar,Y,Ia,Ik,Ak,DomStar)
	
	# Initialize Variables
	DiscreteExtinction = False
	Temp = 0
	
	while DiscreteExtinction == False and Temp == 0:
	
		# Build Admissible Domination Expanded Network
		(nrDom,IaDom,IkDom,AkDom,NDom) = DominationExpandedNetwork(nr,ns,nc,Y,Ia,Ik,Ak,Dom)
	
		# Cycle over Forests, Check if Balanced
		(RF,DiscreteExtinction,XStar) = CycleForests(ns,nr,nc,nrDom,X,Y,IaDom,IkDom,Ik,eps)
		
		if DiscreteExtinction == False:

			# Expand X
			(X,Dom) = FindAdmissibleDom(nr,ns,nc,XStar,Y,Ia,Ik,Ak,DomStar)
			
			# Abort Cycle is X has full support
			if X == [1 for i in range(nc)]:
				Temp = 1

	# Determine Source/Product Only Complexes
	IsSourceOnly = False
	IsProductOnly = False
	sourceonly = []
	productonly = []
	if DiscreteExtinction == True:
		(IsSourceOnly,IsProductOnly,sourceonly,productonly) = IsSourceProduct(crn,nr,ns,nc,Y,Ia,RF)
	
	# Write Output in .dat file
	WriteOutput(IsSourceOnly,IsProductOnly,sourceonly,productonly,DiscreteExtinction,crn,ns,nr,nc,nrDom,RF,X,Y,IaDom,IkDom,eps,file)

# Close file
file.close()