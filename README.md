"# My project's README" 

Discrete Extinctions in Chemical Reaction Networks with Discrete State Spaces

To use, download the files "DE\_Modules.py" and "DE\_Run.py" into common folder with CRN Python package (https://github.com/etonello/crnpy). Running "DE\_Run.py" executes the discrete extinction algorithm on "_filename_" and creates an output file "_filename_.dat". The input file "example\_envz\_ompr" is available as a test. To change the input file, change the value of "_filename_" within "Run\_DE.py".

Reaction networks may be created manually or from a database using CRN Python. To created manually, follow the format:

    reaction_1: species_1 + species_2 -> species_3 + species_4
	reaction_2: species_5 <-> species_6
	...
	
Files can be saved in any text format although the extension will have to match "DE\_Run.py".