import sys
import os
sys.path.insert(0, os.path.join(
    os.path.dirname(os.path.realpath(__file__)), 'crn_python'))

from pulp import *
import numpy as np
from crn import *


def CreateModel(crn):

	ns = crn.n_species
	nc = crn.n_complexes
	nr = crn.n_reactions
	Y = [[crn.complex_matrix[i,j] for j in range(nc)] for i in range(ns)]
	Ia = [[crn.incidence_matrix[i,j] for j in range(nr)] for i in range(nc)]
	Ik = [[0 for j in range(nc)] for i in range(nr)]
	N = [[0 for j in range(nr)] for i in range(ns)]
	Ak = [[0 for j in range(nc)] for i in range(nc)]
	for i in range(nc):
		for j in range(nr):
			if Ia[i][j] < 0: Ik[j][i] = 1
			else: Ik[j][i] = 0
	for i in range(ns):
		for j in range(nr):
			N[i][j] = sum([Y[i][k]*Ia[k][j] for k in range(nc)])
	for i in range(nc):
		for j in range(nc):
			Ak[i][j] = sum([Ia[i][k]*Ik[k][j] for k in range(nr)])
			
	return (ns,nc,nr,Y,Ia,Ik,N,Ak)


def CheckSubconservative(ns,nc,nr,Y,Ia,N,file):

	prob = LpProblem("Check_Conservative",LpMinimize)
	C = LpVariable.dicts("Conservation_Vector",(range(ns)),0,1)
	Z = LpVariable("Temp",0,None)
	prob += -Z, "Objective Function"
	for i in range(ns):
		prob += Z <= C[i], "Constraint 1 " + str(i)
	for i in range(nr):
		prob += lpSum([C[j]*N[j][i] for j in range(ns)]) <= 0, "Constraint 2 " + str(i)
	prob.writeLP("Test.lp")
	prob.solve()
	IsConservative = False
	if value(prob.objective) == 0:
		IsSubconservative = False
		print "The network is not subconservative."
		file.write("The network is not subconservative.\n")
	else:
		IsSubconservative = True
		IsConservative = CheckConservative(ns,nc,nr,Y,Ia,N)
		if IsConservative == True:
			print "The network is conservative."
			file.write("The network is conservative.\n")
		else:
			print "The network is subconservative."
			file.write("The network is subconservative.\n")
		
	return (IsSubconservative,IsConservative)
	
def CheckConservative(ns,nc,nr,Y,Ia,N):

	prob = LpProblem("Check_Conservative",LpMinimize)
	C = LpVariable.dicts("Conservation_Vector",(range(ns)),0,1)
	Z = LpVariable("Temp",0,None)
	prob += -Z, "Objective Function"
	for i in range(ns):
		prob += Z <= C[i], "Constraint 1 " + str(i)
	for i in range(nr):
		prob += lpSum([C[j]*N[j][i] for j in range(ns)]) == 0, "Constraint 2 " + str(i)
	prob.writeLP("Test.lp")
	prob.solve()
	if value(prob.objective) == 0:
		IsConservative = False
	else:
		IsConservative = True
		
	return IsConservative
	

def DominationSet(ns,nc,Y):

	Dom = [[0 for i in range(nc)] for j in range(nc)]
	for i in range(nc):
		for j in range(nc):
			if i != j:
				temp = 0
				for k in range(ns):
					if Y[k][j] >= Y[k][i]:
						temp += 1
				if temp == ns:
					Dom[i][j] = 1

	return Dom
	
def DominationExpandedNetwork(nr,ns,nc,Y,Ia,Ik,Ak,Dom):

	IaDom = [[0 for i in range(nr)] for j in range(nc)]
	for i in range(nc):
		for j in range(nr):
			IaDom[i][j] = Ia[i][j]
	IkDom = [[0 for i in range(nc)] for j in range(nr)]
	for i in range(nr):
		for j in range(nc):
			IkDom[i][j] = Ik[i][j]
	AkDom = [[0 for i in range(nc)] for j in range(nc)]
	AkTemp = [[0 for i in range(nc)] for j in range(nc)]
	for i in range(nc):
		for j in range(nc):
			IaTemp = [0 for k in range(nc)]
			IkTemp = [0 for k in range(nc)]
			if i != j and Dom[i][j] == 1:
				IaDom[j] += [-1]
				IaDom[i] += [1]
				for k in range(nc):
					if (k != i and k != j):
						IaDom[k] += [0]
				IkTemp[j] = 1
				IkDom += [IkTemp]
				AkTemp[i][j] = 1
				AkTemp[j][j] += -1
	for i in range(nc):
		for j in range(nc):
			AkDom[i][j] = Ak[i][j] + AkTemp[i][j]
	nrDom = len(IkDom)
	NDom = [[0 for j in range(nrDom)] for i in range(ns)]
	for i in range(ns):
		for j in range(nrDom):
			NDom[i][j] = sum([Y[i][k]*IaDom[k][j] for k in range(nc)])

	return (nrDom,IaDom,IkDom,AkDom,NDom)
	
	
def FindTerm(nc,Ak):

	prob = LpProblem("Find_Terminal",LpMinimize)
	B = LpVariable.dicts("B",(range(nc)),0,1)
	prob += -lpSum([B[i] for i in range(nc)]), "Objective Function"
	for i in range(nc):
		prob += lpSum([Ak[i][j]*B[j] for j in range(nc)]) == 0, "Constraint 2 " + str(i)
	prob.writeLP("Test.lp")
	prob.solve()
	Term = [0 for i in range(nc)]
	for i in range(nc):
		if B[i].varValue >	0: Term[i] = 1

	return Term
	
def FindAdmissibleDom(nr,ns,nc,Xstar,Y,Ia,Ik,Ak,Dom):

	Temp = 0
	while Temp == 0:
		Xtemp = [0 for i in range(nc)]
		for i in range(nc):
			Xtemp[i] = Xstar[i]
		for i in range(nc):
			for j in range(nc):
				if Xstar[i] == 1 and Xstar[j] == 0 and Ak[j][i] >= 1:
					Xstar[j] = 1
		if Xstar == Xtemp:
			Temp = 1
		
	Temp = 0
	Xtemp = [0 for i in range(nc)]
	while Temp == 0:
		Xtemp = [0 for i in range(nc)]
		for i in range(nc):
			Xtemp[i] = Xstar[i]
		(nrDom,IaDom,IkDom,AkDom,NDom) = DominationExpandedNetwork(nr,ns,nc,Y,Ia,Ik,Ak,Dom)
		Term = FindTerm(nc,AkDom)
		for i in range(nc):
			if Term[i] == 1:
				Xstar[i] = 1
		for i in range(nc):
			if Xstar[i] == 1:
				for j in range(nc):
					Dom[i][j] = 0
		if Xstar == Xtemp:
			Temp = 1

	return (Xstar,Dom)
	
def Rho(nc,nrDom,IkDom,X):

	RhoDom = []
	for i in range(nc):
		RhoTemp = []
		if X[i] == 0:
			for j in range(nrDom):
				if IkDom[j][i] == 1:
					RhoTemp = RhoTemp + [j]
		RhoDom = RhoDom + [RhoTemp]

	return RhoDom
	
def CheckForest(nc,nrDom,X,IaDom,RF,eps):

	prob = LpProblem("Check_Forest",LpMinimize)
	Tree = LpVariable.dicts("Tree",(range(nrDom)),0,None)
	prob += -lpSum([Tree[i] for i in range(nrDom)]), "Objective Function"
	for i in range(nrDom):
		prob += eps*RF[i] - Tree[i] <= 0, 'Constraint 1 ' + str(i)
		prob += Tree[i] - (1/eps)*RF[i] <= 0, 'Constraint 2 ' + str(i)
	for i in range(nc):
		prob += lpSum([IaDom[i][j]*Tree[j] for j in range(nrDom)]) <= -eps + (1/eps)*X[i], 'Constraint 3 ' + str(i)
	prob.writeLP("Test.lp")
	prob.solve()
	if LpStatus[prob.status] == "Infeasible":
		IsForest = False
	else:
		IsForest = True
		
	return IsForest
	
def CheckBalanced(ns,nr,nc,nrDom,RF,X,Y,IaDom,IkDom,eps):

	VX = []
	for i in range(nr):
		if sum([IkDom[i][j]*X[j] for j in range(nc)]) == 0:
			VX += [i]
			
	prob = LpProblem("Check_Balanced",LpMinimize)
	V = LpVariable.dicts("V",(range(nrDom)),0,None)
	prob += -lpSum([V[i] for i in VX]), "Objective Function"
	for i in range(nrDom):
		prob += V[i] - RF[i] <= 0, 'Constraint 1' + str(i)
	for i in range(ns):
		prob += lpSum([lpSum([Y[i][j]*IaDom[j][k]*V[k] for k in range(nr)]) for j in range(nc)]) == 0, 'Constraint 4 ' + str(i)
	for i in range(nc):
		prob += lpSum([IaDom[i][j]*V[j] for j in range(nrDom)]) <= (1/eps)*X[i], 'Constraint 5 ' + str(i)
	prob.writeLP("Test.lp")
	prob.solve()
	tempsum = 0
	tempX = sum([X[i] for i in range(nc)])
	for i in range(nr):
		temp = sum([IkDom[i][j]*X[j] for j in range(nc)])
		if temp == 0:
			tempsum += V[i].varValue
	if tempsum == 0 and tempX != nc:
		IsBalanced = False
	else:
		IsBalanced = True
	ExpandedX = [0 for i in range(nc)]
	if IsBalanced == True:
		for i in range(nrDom):
			if V[i].varValue > 0:
				for j in range(nc):
					if IkDom[i][j] > 0:
						ExpandedX[j] = 1
		for i in range(nc):
			if X[i] == 1:
				ExpandedX[i] = 1
		
	return (IsBalanced,ExpandedX)

def CycleForests(ns,nr,nc,nrDom,X,Y,IaDom,IkDom,Ik,eps):
	
	RhoDom = Rho(nc,nrDom,IkDom,X)
	Combinations = 1
	for i in range(nc):
		Combinations = Combinations*np.math.factorial(len(RhoDom[i]))
	DiscreteExtinction = False
	ExpandedX = [0 for i in range(nc)]
	for i in range(Combinations):
		cumlen = 1
		IndexSet = []
		for j in range(nc):
			if len(RhoDom[j]) != 0:
				cumlen = cumlen*len(RhoDom[j])
				temp = (i*cumlen/Combinations)%(len(RhoDom[j]))
				IndexSet = IndexSet + [RhoDom[j][temp]]
		RF = [0 for i in range(nrDom)]
		for i in range(len(IndexSet)):
			RF[IndexSet[i]] = 1
		for i in range(nc):
			for j in range(nr):
				if X[i] == 1 and Ik[j][i] == 1:
					RF[j] = 1
		IsForest = CheckForest(nc,nrDom,X,IaDom,RF,eps)
		if IsForest == True:
			(IsBalanced,ExpandedX) = CheckBalanced(ns,nr,nc,nrDom,RF,X,Y,IaDom,IkDom,eps)
			if IsBalanced == False:
				DiscreteExtinction = True
				break				
	
	return (RF,DiscreteExtinction,ExpandedX)
	
def IsSourceProduct(crn,nr,ns,nc,Y,Ia,RF):

	sourceonly = []
	productonly = []
	IsSourceOnly = False
	IsProductOnly = False
	Is = [[0 for i in range(nr)] for j in range(nc)]
	Ip = [[0 for i in range(nr)] for j in range(nc)]
	for i in range(nc):
		for j in range(nr):
			if Ia[i][j] == -1:
				Is[i][j] = 1
			if Ia[i][j] == 1:
				Ip[i][j] = 1
	Ys = [[0 for i in range(nr)] for j in range(ns)]
	Yp = [[0 for i in range(nr)] for j in range(ns)]
	for i in range(ns):
		for j in range(nr):
			Ys[i][j] = sum([Y[i][k]*Is[k][j] for k in range(nc)])
			Yp[i][j] = sum([Y[i][k]*Ip[k][j] for k in range(nc)])
	for s in range(ns):
		numsource = 0
		numproduct = 0
		for k in range(nr):
			if RF[k] == 1 and Ys[s][k] > 0:
				numsource += 1
			if RF[k] == 1 and Yp[s][k] > 0:
				numproduct += 1
		if numsource == 0:
			productonly += [str(crn.species[s])]
		if numproduct == 0:
			sourceonly += [str(crn.species[s])]
		if len(productonly) > 0:
			IsProductOnly = True
		if len(sourceonly) > 0:
			IsSourceOnly = True

	return (IsSourceOnly,IsProductOnly,sourceonly,productonly)
	
def WriteOutput(IsSourceOnly,IsProductOnly,sourceonly,productonly,DiscreteExtinction,crn,ns,nr,nc,nrDom,RF,X,Y,IaDom,IkDom,eps,file):

	file.write('The complexes are:\n')
	for i in range(nc):
		file.write('C' + str(i+1) + ': ' + str(crn.complexes[i]) + '\n')
	file.write('The following reactions were in the original network:\n')
	for i in range(nr):
		temp_reactant = 0
		temp_product = 0
		for j in range(nc):
			if IaDom[j][i] == -1: temp_reactant = j+1
			if IaDom[j][i] == 1: temp_product = j+1
		file.write('R' + str(i+1) + ': C' + str(temp_reactant) + ' -> C' + str(temp_product) + '\n')
	file.write('\n')

	if DiscreteExtinction == True:
		print "The network has a discrete extinction event."
		file.write("The network has a discrete extinction event.\n\n")
		file.write('The set X is given by:\n')
		for i in range(nc):
			if X[i] == 1:
				file.write('C' + str(i+1) + ', ')
		file.write('\n')
		file.write('The following domination reactions were added to the network:\n')
		for i in range(nr,nrDom):
			temp_reactant = 0
			temp_product = 0
			for j in range(nc):
				if IaDom[j][i] == -1: temp_reactant = j+1
				if IaDom[j][i] == 1: temp_product = j+1
			file.write('R' + str(i+1) + ': C' + str(temp_reactant) + ' ->D C' + str(temp_product) + '\n')
		file.write('The network has the following unbalanced forest:\n')
		for i in range(nrDom):
			if RF[i] == 1:
				if i != nrDom-1:
					file.write('R' + str(i+1) + ', ')
				else:
					file.write('R' + str(i+1))
		file.write('\n')
		file.write('The following complexes go extinct:\n')
		for i in range(nc):
			if X[i] == 0:
				file.write('C' + str(i+1) + ', ')
		if IsSourceOnly == True:
			file.write('\n' + 'The following species are source only: ')
			for i in range(len(sourceonly)):
				file.write(str(sourceonly[i]) + ', ')
		if IsProductOnly == True:
			file.write('\n' + 'The following species are product only: ')
			for i in range(len(productonly)):
				file.write(str(productonly[i]) + ', ')
	else:
		print "It is inconclusive based on the results of the paper whether the network has a discrete extinction event or not."
		file.write("It is inconclusive based on the results of the paper whether the network has a discrete extinction event or not.\n")
		
